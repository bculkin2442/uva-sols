// How many knights on a checkboard?

#include <stdlib.h>
#include <stdio.h>

#include <math.h>

int spmatch(int x, int y, int p) {
	return (x == p) || (y == p);
}

int pmatch(int x, int y, int p1, int p2) {
	return ((x == p1) && (y == p2)) || ((x == p2) && (y == p1));
}

int knightcount(int xSize, int ySize) {
	if(xSize < 3 && ySize < 3) {
		if(xSize == ySize) {
			return xSize * ySize;
		} else if(pmatch(xSize, ySize, 1, 2)) {
			return 2;
		} else if(pmatch(xSize, ySize, 2, 3)) {
			return 4;
		} else if(spmatch(xSize, ySize, 2)) {
			
		}
	}
}


int main(int argc, char** argv) {
	int xSize, ySize;
	xSize = 0;
	ySize = 0;
	
	scanf("%d %d", &xSize, &ySize);
	
	char* ptemplate = "knights may be placed on a %d row %d column board\n";
	char* ftemplate = "%d %s";

	while((xSize != 0) && (ySize != 0)) {
		char* partial_template;
		
		asprintf(&partial_template, ptemplate, xSize, ySize);
		
		if((xSize < 3) && (ySize < 3)) {
			if(xSize == ySize) {
				printf(ftemplate, xSize * ySize, partial_template);
			} else if(pmatch(xSize, ySize, 1, 2)) {
				printf(ftemplate, 2, partial_template);
			} else if(pmatch(xSize, ySize, 2, 3)) {
				printf(ftemplate, 4, partial_template);
			}
		} else if((xSize == ySize) && (xSize == 3)) {
			printf(ftemplate, 5, partial_template);
		} else {
			printf(ftemplate, ceil((xSize * ySize) / 2), partial_template);
		}

		free(partial_template);
		scanf("%d %d", &xSize, &ySize);
	}
}
