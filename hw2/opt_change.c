/*
	Find the optimal way to make change up to a certain amount
*/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int get_denom(int* tot, int to_val, int denom_val);
void make_change_generic(int amt, int denom_count, int* denoms);
void make_change_usd(int amt);

int get_denom(int* tot, int to_val, int denom_val) {
	int r = 0;
	
	while(*tot <= to_val) {
		*tot += denom_val;
		r++;
	}
	
	*tot -= denom_val;
	r--;
	
	return r;
}

void make_change_generic(int amt, int denom_count, int* denoms) {
	int sols[denom_count][denom_count];
	int tot_coins[denom_count];
	
	int tot = 0;
	
	for(int sdenom = 0; sdenom <= denom_count; sdenom++) {
		for(int i = 0; i < sdenom; i++) {
			sols[sdenom][i] = 0;
			printf("0 ");
		}
		
		for(int i = sdenom; i <= denom_count && tot <= amt; i++) {
			int r = get_denom(&tot, amt, denoms[i]);
			tot_coins[sdenom] += r;
			sols[sdenom][i] = r;
			printf("%d ", r);
		}
		
		printf("\n");
		
	}
	
	int* right_sol = (int* )sols[0];
	int min_coins = INT_MAX;
	
	for(int i = 0; i <= denom_count; i++) {
		if(tot_coins[i] < min_coins) {
			right_sol = (int* )sols[i];
			min_coins = tot_coins[i];
		}
	}
	
	for(int i = 0; i <= denom_count; i++) {
		printf("%d ", right_sol[i]);
	}
	
	printf("\n");

}

void make_change_usd(int amt) {
/*	int denom_vals[5] = {50, 25, 10, 5, 1};
	
	int tot = 0;
	
	for(int i = 0; i <= 5 && tot != amt; i++) {
		printf("%d ", get_denom(&tot, amt, denom_vals[i]));
	}
	
	printf("\n");
*/

	int denom_vals[5] = {50, 25, 10, 5, 1};
	
	printf("Making change");
	
	make_change_generic(amt, 5, denom_vals);
}

int main() {
	
	int to_change;
	
	scanf("%d", &to_change);

	printf("Stuff");
	
	while(to_change != 0) {

		printf("Doing things");
		
		make_change_usd(to_change);
		
		scanf("%d", &to_change);
	}
		
	return 0;
}
